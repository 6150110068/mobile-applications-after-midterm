package com.example.phpmysql;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

public class AddProduct extends AppCompatActivity {

    private static final String TAG = "PHPMySQL";
    EditText inputName;
    EditText inputPrice;
    EditText inputDesc;
    Button btnAddProduct;

    private ProgressDialog pDialog;
    boolean status = false;

    String name;
    String price;
    String description;

    int add_status;
    String add_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        inputName = (EditText) findViewById(R.id.inputName);
        inputPrice = (EditText) findViewById(R.id.inputPrice);
        inputDesc = (EditText) findViewById(R.id.inputDesc);

        btnAddProduct = (Button) findViewById(R.id.btnAddProduct);
        final String url = "http://10.0.2.2/android/insert_product.php";
        status = checkNetworkConenction();
        if (status) {
            Toast.makeText(getApplicationContext(), "Network Available",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Network Not Available",
                    Toast.LENGTH_LONG).show();
        }
        btnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = inputName.getText().toString();
                price = inputPrice.getText().toString();
                description = inputDesc.getText().toString();
                new AddNewProduct().execute(url);
            }
        });
    }
    public final boolean checkNetworkConenction() {
        ConnectivityManager ConnectionManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            return true;
        } else {
            return false;
        }
    }
    class AddNewProduct extends AsyncTask<String, Void, Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddProduct.this);
            pDialog.setTitle("Please Wait..");
            pDialog.setMessage("Add Product..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            try {
                URL url = new URL(params[0]);
                String param = "name=" + URLEncoder.encode(name, "UTF-8") +
                        "&price=" + URLEncoder.encode(price, "UTF-8") +
                        "&description=" + URLEncoder.encode(description, "UTF-8");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setFixedLengthStreamingMode(param.getBytes().length);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
                out.print(param);
                out.close();
                int statusCode = urlConnection.getResponseCode();
                if (statusCode == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = convertInputStreamToString(inputStream);
                    parseResult(response);
                    result = 1;
                } else {
                    result = 0;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
        @Override
        protected void onPostExecute(Integer result) {
            pDialog.dismiss();
            if (result == 1) {
                if (add_status == 1) {
                    Toast.makeText(getApplicationContext(),add_message,Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),"Failed to add product : "
                            +add_message,Toast.LENGTH_LONG).show();
                }
            }
        }
        private String convertInputStreamToString(InputStream inputStream) throws
                IOException {
            BufferedReader bufferedReader = new BufferedReader(new
                    InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }

            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }
        private void parseResult(String result) {
            try {
                //for JSON Object data
                JSONObject response = new JSONObject(result);
                add_status = response.optInt("success");
                add_message = response.optString("message");
            } catch (JSONException e) {
                e.printStackTrace();
        }
            }
            }
    }
