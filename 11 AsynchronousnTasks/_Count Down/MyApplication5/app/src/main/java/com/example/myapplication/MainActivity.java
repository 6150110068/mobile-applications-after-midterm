package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
// Starts the CountDownTask
        new CountDownTask().execute();
    }
    private class CountDownTask extends AsyncTask<Void, Integer, Void> {
        // A callback method executed on UI thread on starting the task
        @Override
        protected void onPreExecute() {
// Getting reference to the TextView tv_counter of the layout activity_main
            TextView tvCounter = (TextView) findViewById(R.id.tv_counter);
            tvCounter.setText("*START*");
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 15; i >= 0; i--) {
                try {
                    Thread.sleep(1000);
                    publishProgress(i); // Invokes onProgressUpdate()
                } catch (InterruptedException e) {
                }
            }
            return null; }


        @Override
        protected void onProgressUpdate(Integer... values) {

            TextView tvCounter = (TextView) findViewById(R.id.tv_counter);

            tvCounter.setText(Integer.toString(values[0].intValue()));
        }

        @Override
        protected void onPostExecute(Void result) {

            TextView tvCounter = (TextView) findViewById(R.id.tv_counter);
            tvCounter.setText("*DONE*");
        }
    }
}